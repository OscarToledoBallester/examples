import unittest
from .. import functions
import error

class MainTest(unittest.TestCase):

	def testArguments(self):
		self.assertEqual(functions.check_arg_files("orders.json", "dependencies.txt", "output.json"), error.INPUT_EXT)
		self.assertEqual(functions.check_arg_files("orders.txt", "dependencies.json", "output.json"), error.INPUT_EXT)
		self.assertEqual(functions.check_arg_files("orders.txt", "dependencies.txt", "output.txt"), error.OUTPUT_EXT)

if __name__ == "__main__": 
	unittest.main()