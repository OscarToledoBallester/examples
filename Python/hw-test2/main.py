import sys
import error
import functions

# Define main() function main
def main():
	arg_num = len(sys.argv)
	# Checks if the number of args is correct
	if arg_num == 4:
		order_file_name = sys.argv[1]
		dependencies_file_name = sys.argv[2]
		output_file = sys.argv[3]
		# Checks if the files are correct
		files_ok, error_msg = functions.check_arg_files(order_file_name, dependencies_file_name, output_file)
		if files_ok:
			# Reads the dependencies file and the orders file, build a childs lists and a order lists
			childs, orders = functions.read_dependencies_file(dependencies_file_name, functions.read_order_file(order_file_name))
			# Constructs the json result and remove the childs from the root json
			result = functions.construct_json(orders, functions.clean_childs(orders, childs))
			# Creates the output file and writes the result
			functions.write_output(result, output_file)
		else:
			print (error_msg)
	else:
		print (error.NUM_ARGS)

if __name__== "__main__":
   main()