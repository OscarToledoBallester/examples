import json
import error
import os.path
from os import path

# Define clean_childs() function returns the orders array result removing the orders with parents
def clean_childs(orders, childs):
	result = orders[:]

	for o in result[:]:
		if o[0] in childs:
			result.remove(o)

	return result

# Define construct_json_array() function returns the orders array with JSON format
def construct_json_array(references, orders):
	result = []

	# Checks if the orders array is empty
	if len(orders) > 0:
		# Loops in the orders array looking for a match in the order references array
		for o in orders:
			for  r in references:
				if o[0] == r[0]:
					# When finds a match stores the info from the references array and calls again the function looking for the possible childs
					result.append({"id" : r[0], "name" : r[1],"dependencies" : construct_json_array(references, r[2])})
					break

	return result

# Define construct_json() function return the orders with JSON format
def construct_json(references, orders):	
	return json.dumps({"orders" : construct_json_array(references, orders)}, indent=2)

# Define read_order_file() function read and returns the orders from the orders file
def read_order_file(file):
	orders = []
	order_file = open(file, 'r')
	order_file.readline()

	for line in order_file:
		line_array = line.split(',')
		order_id = line_array[0]
		order_name = line_array[1]
		orders.append((int(order_id), order_name.replace('\n', ''), []))

	order_file.close()

	return orders

# Define read_dependencies_file() function read and returns the orders with their childs and a list of child orders
def read_dependencies_file(file, orders):
	childs = []
	dependencies_file = open(file, 'r')
	dependencies_file.readline()

	for line in dependencies_file:
		line_array = line.split(',')
		order_id = int(line_array[0])
		child_id = int(line_array[1])
		childs.append(child_id)
		for o in orders:
			if o[0] == order_id:
				o[2].append((child_id, "", []))
				break

	dependencies_file.close()

	return (childs, orders)


# Define write_output() function creates and writes the output file with the result
def write_output(result, file_name):
	f = open(file_name, "w+")
	f.write(result)
	f.close()

# Define check_arg_files() function checks if the files are correct
def check_arg_files(order_file_name, dependencies_file_name, output_file):
	
	correct = False
	msg = ""

	# Checks if the input files extension is correct
	if (order_file_name.lower().endswith('.txt') ) and (dependencies_file_name.lower().endswith('.txt')):
		# Checks if the output file extension is correct
		if output_file.lower().endswith('.json'):
			# Checks if the input order file exists
			if path.exists(order_file_name) :
				# Checks if the input dependencies file exists
				if path.exists(dependencies_file_name) :
					correct = True
				else:
					msg = error.DEP_NOT_FOUND
			else:
				msg = error.ORDER_NOT_FOUND
		else:
			msg = error.OUTPUT_EXT
	else:
		msg = error.INPUT_EXT

	return (correct, msg)