package main

import (
	"encoding/json"
	"fmt" 
	"io"  
	"io/ioutil"
	"log"      
	"net/http" 
	"strings"
	"time"
	"sort"
	"github.com/patrickmn/go-cache"
	mux "github.com/gorilla/mux"
)

//Ping structure
type Ping struct {
	Success	string `json:"success"`
}

//Post structure
type Post struct {
	ID			int 		`json:"id"`
	Author		string 		`json:"author"`
	AuthorID	int 		`json:"authorId"`
	Likes		int 		`json:"likes"`
	Popularity	int 		`json:"popularity"`
	Reads		int 		`json:"reads"`
	Tags		[]string 	`json:"tags"`
}

//PostResponse structure
type PostResponse struct {
	Posts	[]Post `json:"posts"`
}

//ErrorResponse structure
type ErrorResponse struct {
	Error	string `json:"error"`
}

//Error Msg
const errMsgTags string = "Tags parameter is required"
const errMsgInvalid string = " parameter is invalid"

//Const URI api access
const uri string = "https://hatchways.io/api/assessment/blog/posts"

//Create the cache variable, setting the default expiration in 6 hours thinking in
//in big blogs with 2 or 3 daily posts if we would know the publication periodicity 
//we could adjust this numbers accurate
var c = cache.New(6*time.Hour, 30*time.Minute)

//ByID implements sort.Interface based on the ID field.
type ByID []Post

func (a ByID) Len() 			int   	{ return len(a) }
func (a ByID) Less(i, j int) 	bool 	{ return a[i].ID < a[j].ID }
func (a ByID) Swap(i, j int)      		{ a[i], a[j] = a[j], a[i] }

//ByReads implements sort.Interface based on the Reads field.
type ByReads []Post

func (a ByReads) Len() 			int    	{ return len(a) }
func (a ByReads) Less(i, j int) bool 	{ return a[i].Reads < a[j].Reads }
func (a ByReads) Swap(i, j int)      	{ a[i], a[j] = a[j], a[i] }

//ByLikes implements sort.Interface based on the Likes field.
type ByLikes []Post

func (a ByLikes) Len() 			int		{ return len(a) }
func (a ByLikes) Less(i, j int) bool 	{ return a[i].Likes < a[j].Likes }
func (a ByLikes) Swap(i, j int)      	{ a[i], a[j] = a[j], a[i] }

//ByPopularity implements sort.Interface based on the Popularity field.
type ByPopularity []Post

func (a ByPopularity) Len() 			int  	{ return len(a) }
func (a ByPopularity) Less(i, j int) 	bool 	{ return a[i].Popularity < a[j].Popularity }
func (a ByPopularity) Swap(i, j int)      		{ a[i], a[j] = a[j], a[i] }

//handleRequests function, manages the endpoints
func handleRequests() {

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/api/ping", getPing).Methods("GET")
	router.HandleFunc("/api/posts", getPosts).Methods("GET")

	direction := ":8080" 
	fmt.Println("Listening in port " + direction)
	log.Fatal(http.ListenAndServe(direction, router))

}

//getPing function, allows to check the api state
func getPing(w http.ResponseWriter, peticion *http.Request) { 
	var res = "true"
	ping := Ping{res}
	resB, _ := json.Marshal(ping)

	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(resB))
}

//getPosts function, returns a JSON with an array of posts 
func getPosts(w http.ResponseWriter, peticion *http.Request) { 
	var sortBy 			= "id"
	var direction 		= "asc"
	var sortByValues 	= []string{"id", "reads", "likes", "popularity"}
	var directionValues = []string{"asc", "desc"}

	//Gets the sortBy param if exists
	if peticion.URL.Query()["sortBy"] != nil {
		sortBy = peticion.URL.Query()["sortBy"][0]
		//Checks if the param value is one of these: id, reads, likes, popularity
		//if not, return the error
		if( !stringInArray(sortBy, sortByValues) ) {
			errorInvalidParameter(w,"sortBy")
			return
		}	
	}

	//Gets the direction param if exists
	if peticion.URL.Query()["direction"] != nil {
		direction = peticion.URL.Query()["direction"][0]
		//Checks if the param value is one of these: desc, asc
		//if not, return the error
		if( !stringInArray(direction, directionValues) ) {
			errorInvalidParameter(w,"direction")
			return
		}			
	}

	//Gets the tags params if exists, if not return error
	if peticion.URL.Query()["tags"] != nil {
		var results []Post
		tags 		:= 	peticion.URL.Query()["tags"]
		tags 		= 	strings.Split(tags[0], ",")
		timeout 	:= 	time.Duration(8*time.Second)
		client 		:= 	http.Client {
							Timeout: timeout,
						}

		//Loops inside the tags asking the posts associated to each tag
		for _, tag := range tags {
			//Asks for the associated posts in the cache
			postsByTag, found := c.Get(tag)
			
			//If it is empty we ask to the API
			if !found {
				postsByTag = getPostsByTag(tag , client)
				//If the api response is not empty we save the array posts with the tag key
				if len(postsByTag.([]Post)) > 0 {
					c.Set(tag, postsByTag, cache.DefaultExpiration)
				}				
			} 
			
			//Appends the tag associated posts to the main results
			results = append(results, postsByTag.([]Post)...)
		}
		
		//Gives format to the array, checking if there are duplicates and sorting them
		results = formatArrayPost(results, sortBy, direction)
		response := PostResponse{results}

		jsonres, _ := json.MarshalIndent(response, " ", "\t")

		//Return the json
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, string(jsonres))
	} else {
		//Return tag needed error
		errorTagNeeded(w)
		return
	}
}

//errorInvalidParameter function, writes and returns the error in the params sortBy and direction
func errorInvalidParameter(w http.ResponseWriter, parameter string) {
	e := ErrorResponse{parameter + errMsgInvalid}
	resB, _ := json.Marshal(e)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, string(resB))
}

//errorTagNeeded function, writes and returns the error in the param tags
func errorTagNeeded(w http.ResponseWriter) {
	e := ErrorResponse{errMsgTags}
	resB, _ := json.Marshal(e)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, string(resB))
}

//formatArrayPost function, gives format to the array post
func formatArrayPost(posts []Post, sortBy string, direction string) []Post {
	results := deleteDuplicate(posts)
	results = sortPostsArray(results, sortBy, direction)

	return results
}

//getPostsByTag function, asks and returns the post associated to a specific tag
func getPostsByTag(tag string, client http.Client) []Post {
	//Constructs the request
	req, err := http.NewRequest("GET", uri, nil)
	q := req.URL.Query() 
	q.Add("tag", tag) 
	req.URL.RawQuery = q.Encode()
	
	if err != nil {
		log.Fatalln(err)
	}

	//Executes the request
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	//Gives format to the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	
	var result PostResponse
	json.Unmarshal(body, &result)

	return result.Posts
}

//sortPostsArray function, returns the posts sorted by the params
func sortPostsArray(posts []Post, sortBy string, direction string) []Post {
	results := []Post{}

	//Checks the sort direction
	if direction == "asc" {
		results = sortPostsArrayAsc(posts, sortBy)
	} else {
		results = sortPostsArrayDesc(posts, sortBy)
	}

	return results
}

//sortPostsArrayAsc function, returns the posts sorted by the params asc
func sortPostsArrayAsc(posts []Post, sortBy string) []Post {
	result := posts

	switch sortBy {
		case "reads":		sort.Sort(ByReads(result))
		case "likes": 		sort.Sort(ByLikes(result))
		case "popularity": 	sort.Sort(ByPopularity(result))
		default: 			sort.Sort(ByID(result))
	}	

	return result
}

//sortPostsArrayDesc function, returns the posts sorted by the params desc
func sortPostsArrayDesc(posts []Post, sortBy string) []Post {
	result := posts

	switch sortBy {
		case "reads":		sort.Sort(sort.Reverse(ByReads(result)))
		case "likes": 		sort.Sort(sort.Reverse(ByLikes(result)))
		case "popularity": 	sort.Sort(sort.Reverse(ByPopularity(result)))
		default: 			sort.Sort(sort.Reverse(ByID(result)))
	}
	

	return result
}

//deleteDuplicate function, deletes the duplicated post in the array
func deleteDuplicate(posts []Post) []Post {
	result := []Post{}
	keys := make(map[int]bool) 

	for _ , i := range posts {
        if _, value := keys[i.ID]; !value { 
            keys[i.ID] = true
            result = append(result, i) 
        } 
    }
    return result
}

//stringInArray function, checks if the string param is inside the string array
func stringInArray(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

//main function, handles the request received
func main() {
	handleRequests()
}