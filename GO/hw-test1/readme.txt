Auth: Oscar Toledo Ballester - 0sk4rto@gmail.com

Install dependencies:
Go -> https://golang.org/doc/install
github.com/patrickmn/go-cache -> go get github.com/patrickmn/go-cache
github.com/gorilla/mux -> go get -u github.com/gorilla/mux

How to run it:
go run .\main.go

Port:8080

Tests:
Are a Postman collection, so you can import it in Postman and run it.
