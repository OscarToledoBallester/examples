<?php
  /**
  * Main file of the project. Includes the form needed to ask the elections and calls search.php
  *
  * @author -
  * @version 02-07-2020
  */

  require_once 'resources/text.php';
  require_once 'resources/constants.php';
  require_once 'resources/states.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
    <title><?php echo $textTitle[$LANG] ?></title>
    <link rel='stylesheet' href='/stylesheets/style.css' />
  </head>
  <body>
    <div class="address-form">
      <h1><?php echo $textTitle[$LANG] ?></h1>
      <form action="search.php" method="post">
        <p><?php echo $textDescription[$LANG] ?></p>
        <div>
          <label for="street-field"><?php echo $textStreet[$LANG] ?>:</label>
          <input id="street-field" name="street" type="text">
        </div>
        <div>
          <label for="street-2-field"><?php echo $textStreet2[$LANG] ?>:</label>
          <input id="street-2-field" name="street-2" type="text">
        </div>
        <div>
          <label for="city-field"><?php echo $textCity[$LANG] ?>:</label>
          <input id="city-field" name="city" type="text">
    
          <label for="state-field"><?php echo $textState[$LANG] ?>:</label>
          <select id="state-field" name="state" required>
            <option></option>
            <?php 
              foreach ($postalAbbreviations as $pa) {
                  echo '<option value="'.$pa.'">'.$pa.'</option>';
              }
            ?>
          </select>
          <label for="zip-field"><?php echo $textZip[$LANG] ?>:</label>
          <input id="zip-field" name="zip" size="10" type="text">
        </div>
        <div class="button">
          <button type="submit"><?php echo $textSearch[$LANG] ?></button>
        </div>
      </form>
    </div>
  </body>
</html>