<?php
	/**
	* This file keeps all the functions needed to build the ocd-id.
	*
	* @author -
	* @version 02-10-2020
	*/

	/**
	* Returns an ocd-id with place key using in the given parameters
	*
	* @access public
	* @param string $country country specified in the address
	* @param string $state state specified in the address
	* @param string $city city specified in the address
	* @return string
	*/
	function create_ocd_id_place($country, $state, $city)
	{
		$ocd = create_ocd_id_state($country,$state) . "/place:$city";

		return $ocd;
	}

	/**
	* Returns an ocd-id with state key using in the given parameters
	*
	* @access public
	* @param string $country country specified in the address
	* @param string $state state specified in the address
	* @return string
	*/
	function create_ocd_id_state($country, $state)
	{
		$ocd = create_ocd_id_country($country) . "/state:$state";

		return $ocd;
	}

	/**
	* Returns an ocd-id with country key using in the given parameters
	*
	* @access public
	* @param string $country country specified in the address
	* @return string
	*/
	function create_ocd_id_country($country)
	{
		$ocd = "ocd-division/country:$country";

		return $ocd;
	}
?>