<?php
	/**
	* This file keeps all the text used in the application.
	*
	* @author -
	* @version 02-10-2020
	*/

	/**
	* Index file title
	* @var array
	*/
	$textTitle 					= array("EN" => "Find my next election");
	/**
	* Index description
	* @var array
	*/
	$textDescription 			= array("EN" => "Enter the address where you are registered to vote");
	/**
	* Form street field
	* @var array
	*/
	$textStreet 				= array("EN" => "Street");
	/**
	* Form street2 field
	* @var array
	*/
	$textStreet2 				= array("EN" => "Street 2");
	/**
	* Form city field
	* @var array
	*/
	$textCity 					= array("EN" => "City");
	/**
	* Form state field
	* @var array
	*/
	$textState 					= array("EN" => "State");
	/**
	* Form ZIP field
	* @var array
	*/
	$textZip 					= array("EN" => "ZIP");
	/**
	* Form button text
	* @var array
	*/
	$textSearch 				= array("EN" => "Search");
	/**
	* Search file title
	* @var array
	*/
	$textResults 				= array("EN" => "Results Summary");
	/**
	* Search file with no results text
	* @var array
	*/
	$textNoMatches 				= array("EN" => "There is no matches for this address.");
	/**
	* Search file cUrl connection fail text
	* @var array
	*/
	$textConnectionError 		= array("EN" => "An error occurred connecting with the API.");
	/**
	* Search file cUrl connection error message header text
	* @var array
	*/
	$textConnectionErrorMessage = array("EN" => "Error message: ");
	/**
	* Search file json response title
	* @var array
	*/
	$textJsonResponseMessage = array("EN" => "JSON Response");
?>