<?php
	/**
	* This file keeps all the functions needed to manage data format.
	*
	* @author -
	* @version 02-10-2020
	*/

	/**
	* Returns the city with the right format for the ocd-id
	*
	* @access public
	* @param string $city city specified in the address
	* @return string
	*/
	function format_city($city)
	{
		return str_replace(' ', '_', strtolower(trim($city)));
	}

	/**
	* Returns the state with the right format for the ocd-id
	*
	* @access public
	* @param string $state state specified in the address
	* @return string
	*/
	function format_state($state)
	{
		return strtolower($state);
	}

	/**
	* Returns the country with the right format for the ocd-id
	*
	* @access public
	* @param string $country state specified in the address
	* @return string
	*/
	function format_country($country)
	{
		return strtolower($country);
	}

	/**
	* Splits the date and the time received from the API and returns only the date.
	*
	* @access public
	* @param string $dateString date with the format given in the API call
	* @return string
	*/
	function parse_date($dateString) {
		$dateFormatted = split("T",$dateString)[0];
		
		return $dateFormatted;
	}
?>