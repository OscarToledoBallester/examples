<?php
	/**
	* This file contains all the constants the application needs.
	*
	* This data are constant at this moment so we use this variables to save it.
	*
	* @author -
	* @version 02-10-2020
	*/

	/**
	* Saves the language used in the application.
	* @var string
	*/
	$LANG 		= "EN";

	/**
	* Saves the only country we have elections data at this moment.
	* @var string
	*/
	$COUNTRY 	= "us";

?>