<?php
	/**
	* The search file is which construct the API call, do the call and returns the result from Democracy Works API.
	* @author -
	* @version 02-10-2020
	*/

	require_once 'format.php';
	require_once 'ocd.php';


	function getElections($country, $state, $place) {
		/**
		* Default return value
		*/
		$response = "Country and State arguments are mandatory";
		$correct  = false;	

		/**
		* Value of country and state check
		*/
		if(!empty($country) && !empty($state)) {
			/**
			* Parts of the API address
			*/
			$formatted_state   = format_state($state);
			$formatted_country = format_country($country);
			$ocdState 		   = create_ocd_id_state($formatted_country, $formatted_state);
			$reqArguments 	   = $ocdState;
			$reqSource 		   = "https://api.turbovote.org/elections/upcoming";
			$reqKey 		   = "district-divisions";

			/**
			* Verification the city variable is not empty
			*/	
			if(!empty($place)) {

				/**
				* Get the place ocd-id and add it to the url arguments
				*/
				$ocdPlace 		= create_ocd_id_place($formatted_country, $formatted_state, format_city($place));
				$reqArguments  .= ",$ocdPlace";
			}
			/**
			* Build the whole url
			*/	
			$reqUrl = $reqSource . "?" . $reqKey . "=" . $reqArguments;

			/**
			* Build the call using cURL library. Add the json accept header, execute the call and then close it
			*/
			$req = curl_init();
			curl_setopt($req, CURLOPT_URL, $reqUrl);
			curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($req, CURLOPT_HTTPHEADER, array('Accept:application/json'));
			
			$api_response 	= curl_exec($req);

			/**
			* If the api_response is false a problem happened in the connection process or the API is not available
			*/
			if($api_response === false)	{
				
				$response = curl_error($req);
			} else {
				/**
				* If everything is ok decode the json response
				*/
				$response = json_decode($api_response, true);
				$correct = true;
			}
			curl_close($req);
		}

		/**
		* Return variable build
		*/
		$complete_response = array("result" => $correct, "message" =>  $response);

		return $complete_response;
	}