<?php
	/**
	* The search file shows the results and call the file responsible of the API connection.
	*
	* @author -
	* @version 02-10-2020
	*/

	require_once 'resources/dw.php';
	require_once 'resources/constants.php';
	require_once 'resources/text.php';

	$body = "<h1>$textResults[$LANG]</h1>";

	/**
	* Verification the state variable is not empty
	*/	
	if (!empty($_POST['state'])) {
		/**
		* Call the function responsible of connect with the api
		*/
		$response = getElections($COUNTRY, $_POST['state'], $_POST['city']);

		/**
		* If the response is false a problem happened in the connection process or the API is not available
		*/
		if($response["result"] == false)
		{
			$body .= $textConnectionError[$LANG] . "<br>";
			$body .= $textConnectionErrorMessage[$LANG] . $response["message"];
		}
		else
		{
			/**
			* If everything is ok we continue
			*/
		    $elections = $response["message"];		    

		    /**
			* Check if there are matches with the address sent
			*/
		    if(count($elections) > 0) {

		    	/**
				* There are results so it shows a summary of it using a list
				*/
	    		$body .= "<ul>"; 
	    		foreach ($elections as $election) {
	    			$body .= "<li>"; 
	    				$election["date"] = parse_date($election["date"]);
	    				$body .= "Id: " . $election["id"] . " | Date: " . $election["date"] . " | Type: " . $election["type"];
	    			$body .= "</li>";
	    		}
	    		$body .= "</ul>"; 

    			/**
				* And here it shows all the data inside the response
				*/
	    		$body .= "<h1>$textJsonResponseMessage[$LANG]</h1>";
    			$body .= "<pre>";
    			$body .= print_r($elections, true);
    			$body .= "</pre>";
		    }else {
		    	/**
				* No results, show the info about the no matches
				*/
		    	$body .= $textNoMatches[$LANG];
		    }		    
		}		
	}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
    <title><?php echo $textTitle[$LANG] ?></title>
    <link rel='stylesheet' href='/stylesheets/style.css' />
  </head>
  <body>
    <?php echo $body; ?>
  </body>
</html>