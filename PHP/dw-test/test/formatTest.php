<?php
	require_once('../src/resources/format.php');

	use PHPUnit\Framework\TestCase;

	class formatTest extends PHPUnit_Framework_TestCase
	{
	     public function testFormatCity(){
	        $this->assertEquals(format_city("LONDRES"), 'londres');
	        $this->assertEquals(format_city("LON DRES"), 'lon_dres');
	        $this->assertEquals(format_city("Londres"), 'londres');
	        $this->assertEquals(format_city("Londres "), 'londres');
	        $this->assertEquals(format_city(" Londres "), 'londres');
	        $this->assertEquals(format_city(" Londres"), 'londres');
	        $this->assertEquals(format_city("Los Angeles"), 'los_angeles');
	        $this->assertEquals(format_city("Los  Angeles"), 'los__angeles');
	        $this->assertEquals(format_city("Los Angeles "), 'los_angeles');
	        $this->assertEquals(format_city(" Los Angeles"), 'los_angeles');
	    	$this->assertEquals(format_city(" Los Angeles "), 'los_angeles');

	     }
	     public function testFormatState(){
	        $this->assertEquals(format_state("az"), 'az');
	        $this->assertEquals(format_state("Az"), 'az');
	        $this->assertEquals(format_state("aZ"), 'az');
	        $this->assertEquals(format_state("AZ"), 'az');
	     }
	     public function testParseDate(){
	        $this->assertEquals(parse_date("2020-01-24T00:00:00Z"), '2020-01-24');
	        $this->assertEquals(parse_date("2020-01-24TT00:00:00Z"), '2020-01-24');
	        $this->assertEquals(parse_date("2020-01-24"), '2020-01-24');
	     }
	}
?>