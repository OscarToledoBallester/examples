<?php
	require_once('../src/resources/dw.php');

	use PHPUnit\Framework\TestCase;

	class formatTest extends PHPUnit_Framework_TestCase
	{
	     public function testConnection(){
	        $this->assertEquals(getElections("us", "MA", "Wayland")["result"], true);
	     }

	     public function testArguments() {
	     	$this->assertEquals(getElections("", "MA", "Wayland")["result"], false);
	     	$this->assertEquals(getElections("us", "", "Wayland")["result"], false);
	     	$this->assertEquals(getElections("us", "MA", "")["result"], true);
	     }

	     public function testNumberElections() {
	     	$this->assertEquals(count(getElections("us", "AZ", "Tempe")["message"]), 2);
	     	$this->assertEquals(count(getElections("us", "FL", "Florida City")["message"]), 2);
	     	$this->assertEquals(count(getElections("us", "SC", "Beautiful Town")["message"]), 1);
	     	$this->assertEquals(count(getElections("us", "GU", "Beautiful Town")["message"]), 0);
	     }
	}
?>