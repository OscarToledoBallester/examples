<?php
	require_once('../src/resources/ocd.php');

	use PHPUnit\Framework\TestCase;

	class ocdTest extends PHPUnit_Framework_TestCase
	{
	     public function testCountry(){
	        $this->assertEquals(create_ocd_id_country("us"), 'ocd-division/country:us');
	     }
	     public function testState(){
	        $this->assertEquals(create_ocd_id_state("us", "az"), 'ocd-division/country:us/state:az');
	        $this->assertEquals(create_ocd_id_state("uk", "az"), 'ocd-division/country:uk/state:az');
	     }
	     public function testPlace(){
	        $this->assertEquals(create_ocd_id_place("us", "az", "chandler"), 'ocd-division/country:us/state:az/place:chandler');
	        $this->assertEquals(create_ocd_id_place("us", "az", "tempe"), 'ocd-division/country:us/state:az/place:tempe');
	     }
	}
?>