<?php
  /**
  * Main file of the project. Includes the form needed to write the product in a file
  *
  * @author -
  * @version 07-24-2020
  */

  require_once 'resources/text.php';
  require_once 'resources/constants.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
    <title><?php echo $text_title[$LANG] ?></title>
    <link rel='stylesheet' href='css/bootstrap.css'  type='text/css'/>
    <link rel='stylesheet' href='css/style.css' type='text/css'/>
  </head>
  <body>
    <div>
      <h1><?php echo $text_title[$LANG] ?></h1>
      <form id="product_form">
        <div>
          <label for="product_name"><?php echo $text_product_name[$LANG] ?>:</label>
          <input id="product_name" name="product_name" type="text">
        </div>
        <div>
          <label for="qty_stock"><?php echo $text_qty_stock[$LANG] ?>:</label>
          <input id="qty_stock" name="qty_stock" type="text">
        </div>
        <div>
          <label for="price_item"><?php echo $text_price_item[$LANG] ?>:</label>
          <input id="price_item" name="price_item" type="text">
        </div>
        <div class="button">
          <button type="submit"><?php echo $text_add[$LANG] ?></button>
        </div>
      </form>
    </div>
    <div id="jsontotable" class="jsontotable"></div>

    <script src='http://code.jquery.com/jquery-1.10.1.min.js'></script>
    <script src='js/bootstrap.js'></script>
    <script src='js/jquery.jsontotable.min.js'></script>
    <script>
      function showProducts() {
        $("#jsontotable").empty();
        $.ajax({
            //contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            type: 'GET',
            url: "getproducts.php",
            //data: {myData: JSON.stringify($('#product_form').serializeArray())},
            //dataType : 'json', 
            success: function (data) { 
              var jsonresponse = JSON.parse(data);
              $.jsontotable(jsonresponse.Product, { id: '#jsontotable', className: 'table table-hover'});
              $('tbody').append('<tr><td>Total</td><td></td><td></td><td></td><td></td><td>' + jsonresponse.Total + '</td></tr>')
            }
        });
      }
      $( window ).on( "load", showProducts );
      $('form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            type: 'POST',
            url: "addproduct.php",
            data: {myData: JSON.stringify($('#product_form').serializeArray())},
            dataType : 'json'
        });
        showProducts();
      });      
    </script>  
  </body>
</html>