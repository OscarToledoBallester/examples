<?php
	/**
	* This file keeps all the text used in the application.
	*
	* @author -
	* @version 07-24-2020
	*/

	/**
	* Index file title
	* @var array
	*/
	$text_title 					= array("EN" => "PHP Skills test");
	/**
	* Form product name field
	* @var array
	*/
	$text_product_name 				= array("EN" => "Product Name");
	/**
	* Form QtyStock field
	* @var array
	*/
	$text_qty_stock 				= array("EN" => "Quantity in Stock");
	/**
	* Form PricePerItem field
	* @var array
	*/
	$text_price_item 			= array("EN" => "Price per Item");
	/**
	* Form button text
	* @var array
	*/
	$text_add 					= array("EN" => "Add");
	/**
	* Results title
	* @var array
	*/
	$text_results 				= array("EN" => "Results Summary");
	/**
	* Results file with no results text
	* @var array
	*/
	$text_no_matches 				= array("EN" => "There is no matches for this address.");
	/**
	* Search file cUrl connection fail text
	* @var array
	*/
	$text_connection_error 		= array("EN" => "An error occurred connecting with the API.");
	/**
	* Search file cUrl connection error message header text
	* @var array
	*/
	$text_connection_error_message = array("EN" => "Error message: ");
	/**
	* Search file json response title
	* @var array
	*/
	$text_json_response_message = array("EN" => "JSON Response");
?>