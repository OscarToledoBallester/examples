<?php
	/**
	* This file contains all the constants the application needs.
	*
	* This data are constant at this moment so we use this variables to save it.
	*
	* @author -
	* @version 7-24-2020
	*/

	/**
	* Saves the language used in the application.
	* @var string
	*/
	$LANG 			= "EN";

	/**
	* Saves the file name
	* @var string
	*/
	$FILE_NAME 		= "data/products.xml";

	
?>