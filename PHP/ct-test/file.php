<?php
	/**
	* 
	*
	* @author -
	* @version 07-24-2020
	*/

	require_once 'resources/constants.php';

	function insert_product($product_name, $qty_stock, $price_item, $date, $time) {			
		try {
			$file 	  = "data/products.xml";
			$xml_file = new DOMDocument();
			$xml_products = $xml_file->createElement("Products");
			$xml_total = $xml_file->createElement("Total");
			$total = 0;
			if (file_exists($file)) {
			    $xml_file->load($file);
			    $xml_products = $xml_file->getElementsByTagName('Products')->item(0);
			    $xml_total = $xml_products->getElementsByTagName('Total')->item(0);

			    $total = floatval($xml_total->firstChild->nodeValue);
			    $xml_products->removeChild($xml_total);
			    $xml_total = $xml_file->createElement("Total");
			}else {
				$xml_product = $xml_file->createElement("Product");
				$xml_product_name = $xml_file->createElement("Name");
				$xml_product_stock = $xml_file->createElement("Stock");
				$xml_product_price = $xml_file->createElement("Price");
				$xml_product_date = $xml_file->createElement("Date");
				$xml_product_time = $xml_file->createElement("Time");
				$xml_product_total = $xml_file->createElement("Subtotal");

				$xml_product_name->appendChild( $xml_file -> createTextNode( "Name" ));
				$xml_product_stock->appendChild( $xml_file -> createTextNode( "Qty Stock" ));
				$xml_product_price->appendChild( $xml_file -> createTextNode( "Price per Item" ));
				$xml_product_date->appendChild( $xml_file -> createTextNode( "Date" ));
				$xml_product_time->appendChild( $xml_file -> createTextNode( "Time" ));
				$xml_product_total->appendChild( $xml_file -> createTextNode( "Subtotal" ));


				$xml_product->appendChild( $xml_product_name );
				$xml_product->appendChild( $xml_product_stock );
				$xml_product->appendChild( $xml_product_price );
				$xml_product->appendChild( $xml_product_date );
				$xml_product->appendChild( $xml_product_time );
				$xml_product->appendChild( $xml_product_total );

				$xml_products->appendChild( $xml_product );
				$xml_file->appendChild( $xml_products );
			}

			$xml_product = $xml_file->createElement("Product");
			$xml_product_name = $xml_file->createElement("Name");
			$xml_product_stock = $xml_file->createElement("Stock");
			$xml_product_price = $xml_file->createElement("Price");
			$xml_product_date = $xml_file->createElement("Date");
			$xml_product_time = $xml_file->createElement("Time");
			$xml_product_total = $xml_file->createElement("Subtotal");

			$xml_product_name->appendChild( $xml_file -> createTextNode( $product_name ));
			$xml_product_stock->appendChild( $xml_file -> createTextNode( $qty_stock ));
			$xml_product_price->appendChild( $xml_file -> createTextNode( $price_item ));
			$xml_product_date->appendChild( $xml_file -> createTextNode( $date ));
			$xml_product_time->appendChild( $xml_file -> createTextNode( $time ));
			$subtotal = floatval($qty_stock) * floatval($price_item);
			$xml_product_total->appendChild( $xml_file -> createTextNode( sprintf("%.2f",$subtotal) ));

			$total += $subtotal;

			$xml_product->appendChild( $xml_product_name );
			$xml_product->appendChild( $xml_product_stock );
			$xml_product->appendChild( $xml_product_price );
			$xml_product->appendChild( $xml_product_date );
			$xml_product->appendChild( $xml_product_time );
			$xml_product->appendChild( $xml_product_total );

			$xml_total-> appendChild($xml_file -> createTextNode( sprintf("%.2f",$total)));
			$xml_products->appendChild( $xml_total );			
			$xml_products->appendChild( $xml_product );
			
			
			if (!file_exists($file)) {
				
				$xml_file->appendChild( $xml_products );
			}

			

			$xml_file->save($file);

			return "Done";
		} catch (Exception $e) {
		    return 'Exception: '.  $e->getMessage(). "\n";
		}
	} 

	function get_products() {
		$file = "data/products.xml";
		$res = "{}";
		if (file_exists($file)) {
			// Read entire file into string 
			$xml_file = file_get_contents($file); 
			  
			$res = json_encode(simplexml_load_string($xml_file)); 
		}
		print_r($res);
	}
?>