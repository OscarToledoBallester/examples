<?php
	/**
	* 
	*
	* @author -
	* @version 07-24-2020
	*/

	require_once 'file.php';
	header('Content-type: application/json');

 	$jsonString = $_POST['myData'];

	$newJsonString = json_decode($jsonString);

	//print_r($newJsonString);
    
	/**
	* Verification the state variable is not empty
	*/	
	if (!empty($newJsonString[0]->value) && !empty($newJsonString[1]->value) && !empty($newJsonString[2]->value)) {
		/**
		* Call the function responsible of write
		*/
		return insert_product($newJsonString[0]->value, $newJsonString[1]->value, $newJsonString[2]->value, date("m/d/Y"), date("h:i:sa"));
	}else	{
		echo "More data needed";
	}
