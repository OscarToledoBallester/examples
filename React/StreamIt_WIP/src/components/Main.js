import React, { Component } from 'react'
import SideMenu from './SideMenu';
import ResultsVideo from './ResultsVideo';
import ResultsTop from './ResultsTop';
import './styles/Main.css';

class Main extends Component {
  constructor(props) {
    super(props);
    this.handler = this.handler.bind(this);
    this.state = {
      initial: true
    };
  }

  handler() {
    this.setState({
      initial: !this.state.initial
    })
  }


  render() {
    let MainSpace;
    if(this.state.initial) {
      MainSpace = <ResultsTop />;
    } else {
      MainSpace = <ResultsVideo />;
    }
    return (
        <div className="Main">
            {MainSpace}
            <SideMenu handler = {this.handler}/>
        </div>
    )
  }
}

export default Main