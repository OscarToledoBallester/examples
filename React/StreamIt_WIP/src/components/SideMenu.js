import React, { Component } from 'react'
import './styles/SideMenu.css';
import SideMenuGames from './SideMenuGames';
import SideMenuFilters from './SideMenuFilters';
import ButtonStart from './ButtonStart';

class SideMenu extends Component {
  render() {
    return (
        <div className="SideMenu">
          <SideMenuGames /> 
          <hr></hr>
          <SideMenuFilters />
          <ButtonStart handler = {this.props.handler}/>
        </div>
    )
  }
}

export default SideMenu