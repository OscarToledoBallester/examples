import React, { Component } from 'react'
import './styles/StreamerDetails.css';

class StreamerDetails extends Component {
  render() {
    return (
        <div className="StreamerDetails">
            <h2>{this.props.name}</h2>
            <p>{this.props.views} views/monthly</p>
            <p>Online: {this.props.online}</p>
        </div>
    )
  }
}

export default StreamerDetails