import React, { Component } from 'react'
import './styles/SideMenuGames.css';
import TextBoxGroup from './TextBoxGroup';

class SideMenuGames extends Component {
  render() {
    return (
        <div className="SideMenuGames">
          <h2>Games</h2>
          <TextBoxGroup name="Games" games={["League of Legends", "World of Warcraft", "Apex Legends"]} />
          <h3>Genres</h3>
          <TextBoxGroup name="Genres" games={["MOBA", "FPS"]} />
          <h3>Platforms</h3>
          <TextBoxGroup name="Platforms" games={["Steam", "Epic Games Store", "Uplay", "Origin"]} />
          <h3>Developers</h3>
          <TextBoxGroup name="Developers" games={["Riot", "EA", "Microsoft"]} />
          <h3>Publishers</h3>
          <TextBoxGroup name="Publishers" games={["Riot", "EA", "Microsoft"]} />
        </div>
    )
  }
}

export default SideMenuGames