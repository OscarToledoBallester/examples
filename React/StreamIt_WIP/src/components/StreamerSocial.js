import React, { Component } from 'react'
import './styles/StreamerSocial.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class StreamerSocial extends Component {
  render() {
    const listSocial = this.props.social.map((s) =>
      <li><a href={s.url}><FontAwesomeIcon icon={['fab', `${s.name}`]} size="2x" /></a></li>
    );

    return (
        <div className="StreamerSocial">
            <ul>
                {listSocial}
            </ul>            
        </div>
    )
  }
}

export default StreamerSocial