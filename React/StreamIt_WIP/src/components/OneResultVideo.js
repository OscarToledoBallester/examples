import React, { Component } from 'react'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import './styles/OneResultVideo.css';

class OneResultVideo extends Component {
  render() {
    return (
      <div className="OneResultVideo">
        <Accordion.Toggle variant="link" className="CardToggle" as={Card.Header} eventKey={`${this.props.platform}-${this.props.name}`}></Accordion.Toggle>
        <div className="VideoWrapper">         
          <iframe className="TopVideoIframe" src={this.props.url} frameborder="0" allowfullscreen="false" scrolling="no" height="378" width="620"></iframe>
        </div> 
      </div>
    )
  }
}

export default OneResultVideo