import React, { Component } from 'react'
import './styles/StreamerGames.css';

class StreamerGames extends Component {
  render() {
    const listGames = this.props.games.map((game) =>
      <li>
        <div className="GamePart"><img src={game.img} /></div>
        <div className="GamePart name">{game.name}</div>
      </li>
    );

    return (
        <div className="StreamerGames">
          <ul>
            {listGames}
          </ul>
        </div>
    )
  }
}

export default StreamerGames