import React, { Component } from 'react'
import './styles/ButtonStart.css';

class ButtonStart extends Component {
  render() {
    return (
        <input onClick = {this.props.handler} className="ButtonStart" value="Start" type="button"></input>
    )
  }
}

export default ButtonStart