import React, { Component } from 'react'
import InitialImg from './InitialImg';
import Main from './Main';


class Home extends Component {
  render() {
    return (
      <div className="Home">
        <InitialImg />
        <Main /> 
      </div>
    )
  }
}

export default Home
