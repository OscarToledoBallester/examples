import React, { Component } from 'react'
import './styles/TopVideo.css';

class TopVideo extends Component {
  render() {
    return (
        <div className="TopVideo">
            <div className="VideoWrapper">
            <iframe
                className="TopVideoIframe"
                src={this.props.url}
                height="378"
                width="620"
                frameborder="0"
                scrolling="no"
                allowfullscreen="false">
            </iframe>
            </div>
        </div>
    )
  }
}

export default TopVideo